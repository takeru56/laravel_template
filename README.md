# Laravel Template
Welcome! Quick start Laravel Application on Docker!

## Versions
**・PHP:   7.1**  
**・MySQL: 5.7**  
**・Nginx**  
**・phpMyAdmin**


## Getting started
### 1.clone & build
````
$ git clone https://takeru56@bitbucket.org/takeru56/laravel_template.git
$ cd laravel_template
$ docker-compose run web -d --build
````    

### 2.environment

```
$ cp .env.example .env
$ docker-compose run web php artisan key:generate 
```

### 3.install node_modules
```
$ docker-compose run web npm install
```
## Access

**URL        => localhost:80/**  

**phpMyAdmin => localhost:8080/**  

